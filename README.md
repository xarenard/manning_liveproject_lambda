```
xavier$ node --version
v12.14.0

xavier$ npm --version
6.13.4

xavier$ serverless --version
Framework Core: 2.13.0
Plugin: 4.1.2
SDK: 2.3.2
Components: 3.4.2

xavier$ serverless invoke -f hello
{
    "statusCode": 200,
    "body": "{\n  \"message\": \"Go Serverless v1.0! Your function executed successfully!\",\n  \"input\": {}\n}"
}
``